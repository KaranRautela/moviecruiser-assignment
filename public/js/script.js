var movies;
var favourites;
var favmovie;


function getMovies() {
	var xhr = new XMLHttpRequest();

	xhr.open("get", "http://localhost:3000/movies");
	xhr.getResponseHeader("Content-Type", "application/json")
	xhr.send(null)
	xhr.onload = function(){
		movies = JSON.parse(xhr.responseText);
		let str = "";
		movies.forEach(function (movie) {
			// str += `<li>${movie.title} </li>
			// <img src=${movie.image} height=200px>
			// <button onclick="addFavourite(${movie.id})">Add To Favourites</button>`
		    str += `<div class="card border-dark  mb-3" style="max-width: 540px;">
			<div class="row g-0">
			  <div class="col-md-4">
				<img src="${movie.image}" class="img-fluid rounded-start" alt="Movie">
			  </div>
			  <div class="col-md-8">
				<div class="card-body">
				  <h5 class="card-title">${movie.title}</h5>
				  <p class="card-text"><small class="text-muted">${movie.year}</small></p>
				  <p class="card-text">${movie.description}</p>
				  <button class="btn btn-primary" onclick="addFavourite(${movie.id})">Add To Favourites</a>
				</div>
			  </div>
			</div>
		  </div>`
		});
		document.getElementById("moviesList").innerHTML = str;
	}

}

function getFavourites() {
	var xhr = new XMLHttpRequest();

	xhr.open("get", "http://localhost:3000/favourites");
	xhr.getResponseHeader("Content-Type", "application/json")
	xhr.send(null)
	xhr.onload = function(){
		favourites = JSON.parse(xhr.responseText);
		let str = "";
		favourites.forEach(function (favmovie) {
		// str += `<li>${favmovie.title} </li><img src=${favmovie.image} height=200px>
		// <button onclick="removeFavourite(${favmovie.id})">Remove from Favourites</button>`
		str += `<div class="card border-dark mb-3" style="max-width: 540px;">
		<div class="row g-0">
		  <div class="col-md-4">
			<img src="${favmovie.image}" class="img-fluid rounded-start" alt="Movie">
		  </div>
		  <div class="col-md-8">
			<div class="card-body">
			  <h5 class="card-title">${favmovie.title}</h5>
			  <p class="card-text"><small class="text-muted">${favmovie.year}</small></p>
			  <p class="card-text">${favmovie.description}</p>
			  <button class="btn btn-danger" onclick="removeFavourite(${favmovie.id})">Remove from Favourites</a>
			</div>
		  </div>
		</div>
	  </div>`
		});
		document.getElementById("favouritesList").innerHTML = str;
	}
}
var duplicate = false;
function addFavourite(movie_id){
	movies.forEach(function (movie) {
		if(movie.id === movie_id){
				favmovie = movie;
			}
		});
	favourites.forEach((fav) =>{
		if(fav.id === movie_id){
			alert("Already Added To Favourites.");
			duplicate = true;
		}	
	});
	if(!duplicate){
		var xhr = new XMLHttpRequest();
		xhr.open("post","http://localhost:3000/favourites");
		xhr.setRequestHeader("Content-Type","application/json");
		// xhr.onload = function(){
		// 	console.log(xhr.responseText);
		// }
		xhr.send(JSON.stringify(favmovie))
	}
	}

function removeFavourite(movie_id){
	var xhr = new XMLHttpRequest();
	xhr.open("delete","http://localhost:3000/favourites/"+String(movie_id));
	// xhr.onload = function(){
	// 	console.log(xhr.responseText);
	// }
	xhr.send(null)
}



getMovies();
getFavourites();



